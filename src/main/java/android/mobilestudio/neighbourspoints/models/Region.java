package android.mobilestudio.neighbourspoints.models;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class Region {
    private LatLng center;
    private double radius;

    public Region(LatLng center, double radius) {
        this.center = center;
        this.radius = radius;
    }
    public Region(LatLng center) {
        this.center = center;
    }

    public LatLng getCenter() {
        return center;
    }

    public void changeCenter(LatLng center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
