package android.mobilestudio.neighbourspoints.models;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.UUID;

public class MapPoint {
    private String uniqueID ;
    private LatLng latLn ;
    private BitmapDescriptor icon ;
    private String title ;
    private long minTimeforUpdate ; // by millisecond
    private int minDistanceForUpdate ; // by meter
    private Date lastUpdateTime ;

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public void setMinDistanceForUpdate(int minDistanceForUpdate) {
        this.minDistanceForUpdate = minDistanceForUpdate;
     }

    public void setMinTimeforUpdate(long minTimeforUpdate) {
        this.minTimeforUpdate = minTimeforUpdate;
    }

    public long getMinTimeforUpdate() {
        return minTimeforUpdate;
    }

    public int getMinDistanceForUpdate() {
        return minDistanceForUpdate;
    }

    public MapPoint() {
        uniqueID = UUID.randomUUID().toString();
     }

    public String getUniqueID() {
        return uniqueID;
    }

    public LatLng getLatLn() {
        return latLn;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public void latLn(LatLng latLn) {
        this.latLn = latLn;
    }
    
    public BitmapDescriptor getIcon() {
        return icon;
    }

    public void icon(int icon) {
        this.icon = BitmapDescriptorFactory.fromResource(icon);
    }
    public void icon(Uri path) {
        this.icon = BitmapDescriptorFactory.fromPath(path.toString());
    }

    public void icon(BitmapDescriptor icon) {
        this.icon = icon;
    }


    public String getTitle() {
        return title;
    }

    public void title(String title) {
        this.title = title;
    }

}
