package android.mobilestudio.neighbourspoints.map.abstractMap;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerClickedListener;
import android.mobilestudio.neighbourspoints.parsers.IParser.IMapPointParser;
import java.util.ArrayList;
import java.util.List;


public abstract class AbstractMap<T,E>{
    protected IMapPointParser<T> pointParser = null ;
    protected  List<E> markers = new ArrayList<>();
    public abstract void setPoints(List<MapPoint> points);
    public abstract void addPoint(MapPoint point);
    public abstract void removePoint (MapPoint point) ;
    public abstract void redrawPoint (MapPoint prePoint , MapPoint currentPoint ) ;
    public abstract void addPoints(List<MapPoint> points);
    public abstract void clearPoints();
    public abstract void onMarkerClick(onMarkerClickedListener<E> listener) ;
    public abstract E getMarker (MapPoint point) ;
}
