package android.mobilestudio.neighbourspoints.CallbackListener;

public interface onMarkerRegionStateChangedListener<T> {
    void onPointEntered(T marker );
    void onPointLeaved (T marker );
}
