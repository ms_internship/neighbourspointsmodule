package android.mobilestudio.neighbourspoints.CallbackListener;

public interface onMarkerLocationUpdateListener<T> {
    void onMarkerLocationUpdated(T marker);
}
