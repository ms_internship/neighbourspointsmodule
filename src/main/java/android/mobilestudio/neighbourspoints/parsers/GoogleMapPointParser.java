package android.mobilestudio.neighbourspoints.parsers;

import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.parsers.IParser.IMapPointParser;

import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapPointParser implements IMapPointParser<MarkerOptions> {

    @Override
    public  MarkerOptions parsePoint(MapPoint point) {
        MarkerOptions options = new MarkerOptions();
        options.icon(point.getIcon());
        options.title(point.getTitle());
        options.position(point.getLatLn());
        return options;
    }
}
