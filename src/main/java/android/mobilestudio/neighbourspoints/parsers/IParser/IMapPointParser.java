package android.mobilestudio.neighbourspoints.parsers.IParser;

import android.mobilestudio.neighbourspoints.models.MapPoint;

public interface IMapPointParser<T> {
    T parsePoint (MapPoint point);
}
