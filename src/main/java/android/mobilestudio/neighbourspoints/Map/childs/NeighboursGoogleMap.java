package android.mobilestudio.neighbourspoints.map.childs;

import android.graphics.Point;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerClickedListener;
import android.mobilestudio.neighbourspoints.map.abstractMap.AbstractMap;
import android.mobilestudio.neighbourspoints.parsers.GoogleMapPointParser;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


public class NeighboursGoogleMap extends AbstractMap<MarkerOptions, Marker> {
    private GoogleMap map;

    public NeighboursGoogleMap(GoogleMap map) {
        this.map = map;
        pointParser = new GoogleMapPointParser();
    }

    @Override
    public void setPoints(List<MapPoint> list) {
        clearPoints();
        addPoints(list);
    }

    @Override
    public void addPoint(MapPoint point) {
        markers.add(map.addMarker(pointParser.parsePoint(point).snippet(point.getUniqueID())));
    }

    @Override
    public void removePoint(MapPoint point) {
          Marker marker = getMarker(point);
          marker.remove();
          markers.remove(marker);
    }
    @Override
    public void redrawPoint(MapPoint prePoint, MapPoint currentPoint) {
        moveAnimation(getMarker(prePoint), currentPoint.getLatLn());
    }

    @Override
    public void clearPoints() {
        for (Marker marker : markers) {
            marker.remove();
        }
        markers.clear();
    }

    @Override
    public void onMarkerClick(final onMarkerClickedListener<Marker> listener) {
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (listener != null) {
                    listener.onMarkerClickListener(marker);
                }
                return true;
            }
        });
    }

    @Override
    public Marker getMarker(MapPoint point) {
        for (Marker marker : markers) {
             if (marker.getSnippet().equals(point.getUniqueID())) {
                return marker;
            }
        }
        return null;
    }

    @Override
    public void addPoints(List<MapPoint> list) {
        for (MapPoint point : list) {
            addPoint(point);
        }
    }

    private void moveAnimation(final Marker marker, final LatLng toPosition) {

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1000;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                LatLng latLng = new LatLng(lat, lng) ;
                Log.v("inter LatLng " , latLng.toString() );
                Log.v("inter LatLng " , toPosition.toString() );
                marker.setPosition(latLng);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
               // marker.setPosition(toPosition);
            }

        });

    }


}
