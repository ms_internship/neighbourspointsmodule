package android.mobilestudio.neighbourspoints.map.factory;


import android.mobilestudio.neighbourspoints.map.childs.NeighboursGoogleMap;
import android.mobilestudio.neighbourspoints.map.abstractMap.AbstractMap;

import com.google.android.gms.maps.GoogleMap;


public class NeighbourMapFactory<T> {

    public AbstractMap create(T map){
        if (map instanceof GoogleMap) {
            return new NeighboursGoogleMap((GoogleMap) map);
        }
        return null;
    }
}
