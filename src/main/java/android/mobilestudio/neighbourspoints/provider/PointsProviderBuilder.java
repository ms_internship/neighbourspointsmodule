package android.mobilestudio.neighbourspoints.provider;

import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerClickedListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerLocationUpdateListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerRegionStateChangedListener;
import android.mobilestudio.neighbourspoints.map.abstractMap.AbstractMap;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.models.Region;

import java.util.List;

public class PointsProviderBuilder  {
    private List<MapPoint> allPoints;
    private AbstractMap map;
    private Region region ;



    private long minTimeforUpdate;
    private int minDistanceForUpdate;
    private onMarkerClickedListener clickedListener ;
    private onMarkerLocationUpdateListener  updateListener;
    private onMarkerRegionStateChangedListener  changeStateListener;

    public long getMinTimeforUpdate() {
        return minTimeforUpdate;
    }

    public PointsProviderBuilder setMinTimeforUpdate(long minTimeforUpdate) {
        this.minTimeforUpdate = minTimeforUpdate;
        return this;
    }

    public int getMinDistanceForUpdate() {
        return minDistanceForUpdate;
    }

    public PointsProviderBuilder setMinDistanceForUpdate(int minDistanceForUpdate) {
        this.minDistanceForUpdate = minDistanceForUpdate;
        return this;
    }

    public PointsProviderBuilder setClickedListener(onMarkerClickedListener clickedListener) {
        this.clickedListener = clickedListener;
        return this;
    }

    public Region getRegion() {
        return region;
    }

    public PointsProviderBuilder setRegion(Region region) {
        this.region = region;
        return  this ;
    }
    public List<MapPoint> getAllPoints() {
        return allPoints;
    }

    public AbstractMap getMap() {
        return map;
    }

    public onMarkerClickedListener  getClickedListener() {
        return clickedListener;
    }

    public onMarkerLocationUpdateListener getUpdateListener() {
        return updateListener;
    }

    public onMarkerRegionStateChangedListener  getChangeStateListener() {
        return changeStateListener;
    }

    public PointsProviderBuilder setUpdateListener(onMarkerLocationUpdateListener  updateListener) {
        this.updateListener = updateListener;
        return this;
    }

    public PointsProviderBuilder() {
        minDistanceForUpdate  = 100 ; // meter
        minTimeforUpdate = 5*60* 1000 ; // 5mins
    }

    public PointsProviderBuilder setChangeStateListener(onMarkerRegionStateChangedListener changeStateListener) {
        this.changeStateListener = changeStateListener;
        return this;
    }


    public PointsProviderBuilder(AbstractMap map, List<MapPoint> allPoints, Region region) {
        this.allPoints = allPoints;
        this.map = map;
        this.region = region;
    }

    public PointsProvider build() {
        return new PointsProvider(this);
    }

}
