package android.mobilestudio.neighbourspoints.provider;

import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerClickedListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerLocationUpdateListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerRegionStateChangedListener;
import android.mobilestudio.neighbourspoints.map.abstractMap.AbstractMap;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.models.Region;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class PointsProvider {

    private List<MapPoint> insidePoints = new ArrayList<>();
    private List<MapPoint> outsidePoints = new ArrayList<>();
    private AbstractMap map;
    private long minTimeforUpdate;
    private int minDistanceForUpdate;

    private RegionFilter filter;
    private MapUpdater updater;

    public PointsProvider(PointsProviderBuilder builder) {
        this.map = builder.getMap();
        this.minDistanceForUpdate = builder.getMinDistanceForUpdate();
        this.minTimeforUpdate = builder.getMinTimeforUpdate();
        map.onMarkerClick(builder.getClickedListener());
        filter = new RegionFilter(builder.getRegion());
        updater = new MapUpdater(map, builder.getUpdateListener(), builder.getChangeStateListener());
        addPointsToMap(builder.getAllPoints());
    }

    public void addPoint(MapPoint point) {
        filter.filterPoint(point, insidePoints, outsidePoints);
    }

    public void setClickedListener(onMarkerClickedListener clickedListener) {
        map.onMarkerClick(clickedListener);
    }

    public void setUpdateListener(onMarkerLocationUpdateListener updateListener) {
         updater.setUpdateListener(updateListener);
    }

    public void setChangeStateListener(onMarkerRegionStateChangedListener changeStateListener) {
         updater.setChangeStateListener(changeStateListener);
    }

    public void OnMarkerLocationUpdated(MapPoint point) {
        updater.OnMarkerLocationUpdated(point, insidePoints, outsidePoints, filter);
    }

    public void changeCenterOfRegion(LatLng center) {
        filter = new RegionFilter(new Region(center, filter.getRegion().getRadius()));
        updater.reconstructPoints(filter, insidePoints, outsidePoints);
    }

    public void changeRadiusOfRegion(Double radius) {
        filter = new RegionFilter(new Region(filter.getRegion().getCenter(), radius));
        updater.reconstructPoints(filter, insidePoints, outsidePoints);
    }

    private void addPointsToMap(List<MapPoint> list) {
        filter.filterPoints(addAttrUpdate(list), insidePoints, outsidePoints);
        map.setPoints(insidePoints);
    }
    private List<MapPoint> addAttrUpdate (List<MapPoint> list)
    {
        for(MapPoint point: list){
            point.setMinDistanceForUpdate(minDistanceForUpdate);
            point.setMinTimeforUpdate(minTimeforUpdate);
        }
        return list ;
    }

}



