package android.mobilestudio.neighbourspoints.provider;

import android.mobilestudio.neighbourspoints.models.MapPoint;
import java.util.List;


enum MovementCase {

    InsideToInside ,InsideToOutside , OutsideToInside , OutsideToOutside , nothing ;
    public MovementCase getCase (MapPoint point , List<MapPoint> inside , List<MapPoint> outside, RegionFilter filter){
            MapPoint prePoint = inList(inside, point);
            if (prePoint != null) {
                if (filter.isInsideRegion(point.getLatLn())) {
                    return InsideToInside ;
                } else {
                     return InsideToOutside;
                }
            }
            prePoint = inList(outside, point);
            if (prePoint != null) {
                if (filter.isInsideRegion(point.getLatLn())) {
                    return OutsideToInside ;
                } else {
                    return OutsideToOutside ;
                }
            }
            return InsideToInside ;

        }
    public MapPoint inList(List<MapPoint> list, MapPoint mapPoint) {
        for (MapPoint point : list) {
            if (point.getUniqueID().equals(mapPoint.getUniqueID())) {
                return point;
            }
        }
        return null;
    }
}
