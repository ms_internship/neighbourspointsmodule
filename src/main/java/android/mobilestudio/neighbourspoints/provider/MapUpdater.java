package android.mobilestudio.neighbourspoints.provider;

import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerLocationUpdateListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerRegionStateChangedListener;
import android.mobilestudio.neighbourspoints.map.abstractMap.AbstractMap;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.models.Region;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class MapUpdater {

    private AbstractMap map;
    private onMarkerLocationUpdateListener updateListener;
    private onMarkerRegionStateChangedListener changeStateListener;

    public MapUpdater(AbstractMap map, onMarkerLocationUpdateListener updateListener, onMarkerRegionStateChangedListener changeStateListener) {
        this.updateListener = updateListener;
        this.changeStateListener = changeStateListener;
        this.map = map;
    }
    public void setChangeStateListener(onMarkerRegionStateChangedListener changeStateListener) {
        this.changeStateListener = changeStateListener;
    }

    public void setUpdateListener(onMarkerLocationUpdateListener updateListener) {
        this.updateListener = updateListener;
    }

    public void OnMarkerLocationUpdated(MapPoint point, List<MapPoint> insidePoints, List<MapPoint> outsidePoints, RegionFilter filter) {
        switch (MovementCase.nothing.getCase(point, insidePoints, outsidePoints, filter)) {
            case InsideToInside:
                insideToInside(getPoint(insidePoints, point), point, insidePoints);
                break;
            case InsideToOutside:
                insideToOutside(getPoint(insidePoints, point), point, insidePoints, outsidePoints);
                break;
            case OutsideToInside:
                outsideToInside(getPoint(outsidePoints, point), point, insidePoints, outsidePoints);
                break;
            case OutsideToOutside:
                outsideToOutside(getPoint(outsidePoints, point), point, outsidePoints);
                break;
        }
    }

    public void reconstructPoints (RegionFilter filter, List<MapPoint> insidePoints, List<MapPoint> outsidePoints) {
        onMarkerLocationUpdateListener listener = updateListener;
        updateListener = null;
        for (MapPoint point : insidePoints) {
            OnMarkerLocationUpdated(point, insidePoints, outsidePoints, filter);
        }
        for (MapPoint point : outsidePoints) {
            OnMarkerLocationUpdated(point, insidePoints, outsidePoints, filter);
        }
        updateListener = listener;
    }

    private void insideToInside(MapPoint prePoint, MapPoint point, List<MapPoint> insidePoints) {
        // notify map to redraw
        map.redrawPoint(prePoint, point);
        //   then i'll notify point updated
        insidePoints.remove(prePoint);
        insidePoints.add(point);

        if (updateListener != null) {
            updateListener.onMarkerLocationUpdated(map.getMarker(point));
        }
    }

    private void insideToOutside(MapPoint prePoint, MapPoint point, List<MapPoint> insidePoints, List<MapPoint> outsidePoints) {
        if (changeStateListener != null) {
            // notify there is point Leaves
            changeStateListener.onPointLeaved(map.getMarker(prePoint));
        }
        // sent to map to remove this marker
        map.removePoint(prePoint);
        // update in the Lists
        outsidePoints.add(point);
        insidePoints.remove(prePoint);
    }

    private void outsideToInside(MapPoint prePoint, MapPoint point, List<MapPoint> insidePoints, List<MapPoint> outsidePoints) {
        map.addPoint(point);
        //   notify map to add point
        // update in the Lists
        outsidePoints.remove(prePoint);
        insidePoints.add(point);
        if (changeStateListener != null) {
            changeStateListener.onPointEntered(map.getMarker(point));
        }
    }

    private void outsideToOutside(MapPoint prePoint, MapPoint point, List<MapPoint> outsidePoints) {
        //  don't anything
        outsidePoints.remove(prePoint);
        outsidePoints.add(point);
    }

    public MapPoint getPoint(List<MapPoint> list, MapPoint mapPoint) {
        for (MapPoint point : list) {
            if (point.getUniqueID().equals(mapPoint.getUniqueID())) {
                return point;
            }
        }
        return null;
    }

}
