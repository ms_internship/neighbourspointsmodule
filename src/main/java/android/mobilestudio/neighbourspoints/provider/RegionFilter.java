package android.mobilestudio.neighbourspoints.provider;

import android.location.Location;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.models.Region;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

class RegionFilter {
    private Region region ;



    RegionFilter(Region region) {
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
    void filterPoints(List<MapPoint> list, List<MapPoint> insidePoints, List<MapPoint> outsidePoints) {
        for (MapPoint point : list) {
            filterPoint(point,insidePoints,outsidePoints);
        }
    }
    public void filterPoint(MapPoint point,List<MapPoint> insidePoints , List<MapPoint> outsidePoints) {
        if (isInsideRegion(point.getLatLn())) {
            outsidePoints.add(point);
        } else {
            insidePoints.add(point);
         }
    }

    public boolean isInsideRegion(LatLng src ) {
        return (getDistanceBtnTwoPoints(src) <= region.getRadius());
    }

    private float getDistanceBtnTwoPoints(LatLng src ) {
        float[] distance = new float[1];
        Location.distanceBetween(src.latitude, src.longitude, region.getCenter().latitude, region.getCenter().longitude, distance);
        return distance[0] ;
    }
}
